# Backend Custom Product Import/Update Plugin Work Sample

This repository contains everything you will need to create your work sample when applying as a developer at Intervaro.

The work sample will focus on building a plugin for WordPress + WooCommerce.

## The plugin
You should create a custom plugin that imports or updates products from an [external REST API](https://retoolapi.dev/AqgXOl/intervaro).

You should use a pagination so the import only imports/updates maximum of 10 products at a time, or else the server will timeout.

### Other requirements:
- An [explanatory screenshot can be found here](https://i.postimg.cc/PfxDJqWB/work-sample-woocommerce.png) for details on what fields should be populated from the REST API.
- An Options Page with buttons for importing or updating products
- After import is complete an admin notice with a summary of following should appear:
    - Number of products imported
    - Number of products updated
- Follow [WordPress Coding Standards](https://developer.wordpress.org/coding-standards/wordpress-coding-standards/)

## Expectations
We expect you to build a custom plugin from the ground up without libraries or packages. You can advantageously use functions from WordPress for remote requests and other stuff. The finished work sample should have a readme describing how to get the plugin up and running.

We will test your implementation in a clean install of WordPress+WooCommerce with the theme [Storefront](https://woocommerce.com/storefront/) installed and activated.


## If you have the time
The following items are not required but if you have the time feel free to implement them. Do note though that we will review these extras in the same way we review the base work sample.

- Possibility to update a product through a ”Update product”-button on the Product Edit Page
- Possibility to import/update products without images through additional buttons on the Options Page 

## Data source
You can find the REST API here: [https://retoolapi.dev/AqgXOl/intervaro](https://retoolapi.dev/AqgXOl/intervaro)

### Please note
- All products in the API should be imported as _Simple product_.
- Some fields are intentionally left empty.

### HTTP method endpoints examples:
- GET filter: /AqgXOl/intervaro?sku=value
- GET by id: /AqgXOl/intervaro/1
- GET paginate: /AqgXOl/intervaro?_page=2&_limit=10


## Have fun!
This work sample is not about pointing fingers if something is off. We want to see how you think and how you adapt requirements into code.
